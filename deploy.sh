#!/bin/bash


# Debug Flag
#set -x

# set variables
DOCKER_MACHINE_NAME='landing-approver'
LOG_DIRECTORY='../logs'

[ -d $LOG_DIRECTORY ] || mkdir $LOG_DIRECTORY

# start the docker machine
docker-machine start $DOCKER_MACHINE_NAME
DOCKER_MACHINE_IP=$(docker-machine ip $DOCKER_MACHINE_NAME)
echo "IP of docker machine: $DOCKER_MACHINE_IP"

# stop running containers
docker-compose stop

# Clean up existing containers and images
docker-compose rm -f
docker rmi -f $(docker images -q) 

# Build new jars
./gradlew build

# Start docker machines
docker-compose up | tee $LOG_DIRECTORY/flightapprover_$(date +%b_%d_%Y_%H_%M_%S).log
