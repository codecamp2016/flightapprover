CREATE DATABASE codecamp;
GRANT USAGE ON *.* TO codecamp@localhost IDENTIFIED BY 'codecamp';
GRANT ALL PRIVILEGES ON codecamp.* TO codecamp@localhost;
FLUSH PRIVILEGES;