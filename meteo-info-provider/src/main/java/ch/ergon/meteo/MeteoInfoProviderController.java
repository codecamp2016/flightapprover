package ch.ergon.meteo;

import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class MeteoInfoProviderController {

	@Autowired
	private MeteoInformationRepository repository;

	@ApiOperation(value = "get meteo info for date")
	@RequestMapping(value = "/meteo-info/{icaoCode}", method = RequestMethod.GET)
	public MeteoInformationTO getMeteoInfoForDateTime(@PathVariable @ApiParam(defaultValue = "LSZH") String icaoCode,
			@RequestParam(value = "datetime", required = false) @ApiParam(defaultValue = "2014-04-23T04:30:45.123Z") @DateTimeFormat(iso = ISO.DATE_TIME) ZonedDateTime dateTime) {
		MeteoInformationTO result = null;
		if (dateTime != null) {
			result = repository.getMeteoInformationByIcaoCodeAndTime(icaoCode, dateTime);
		} else {
			result = repository.getMeteoInformationByIcaoCode(icaoCode);
		}
		return result;
	}

}
