package ch.ergon.meteo;

import java.sql.Date;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.springframework.stereotype.Repository;

@Repository

public class MeteoInformationRepository {

	public MeteoInformationTO getMeteoInformationByIcaoCodeAndTime(String icaoCode, ZonedDateTime time) {
		return getMeteoInformationByIcaoCode(icaoCode);
	}

	public MeteoInformationTO getMeteoInformationByIcaoCode(String icaoCode) {
		return new MeteoInformationTO(icaoCode, Date.from(ZonedDateTime.now(ZoneOffset.UTC).toInstant()), 30, 250);

	}

}
