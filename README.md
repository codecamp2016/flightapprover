# Zweck
Dieses Repo bietet eine Ausgangslage, um im CodeCamp 2016 mit dem Microservice-Projekt zu starten.

# Vorbedingungen
* Java 8
* Mysql installiert (bzw. MariaDB) zB via XAMPP Paket https://www.apachefriends.org/de/index.html
* DB erstellt
    * z.B. über http://localhost/phpmyadmin
    * DB User codecamp mit hostname localhost und pw codecamp
    * DB Schema dazu mit dem Namen codecamp , auf welchem der Benutzer codecamp die vollen Rechte hat.

# Struktur
Das Projekt besteht aus zwei Microservices:
* hello-service: Dieser Service besteht aus einem Rest-Endpoint (ohne DB) und einem HTML UI.
* user-service: Dieser Service bietet neben mehreren Rest-Endpoints die Persistierung der user innerhalb einer DB an.

Zusätzlich ist das Projekt common eingebunden, welches die Objektklasse anbietet, die von beiden Microservices genutzt wird.

# Eclipse Integration
Die Eclipse Projekt Files sind nicht im Repo enthalten und können folgendermassen erstellt werden.

* ins Root Verzeichnis wechseln
* ./gradlew eclipse aufrufen
* in eclipse via Import -> Existing Projects into Workspace 
    * das Root Verzeichnis auswählen
    * "Search for nested projects" anwählen
    * die Projekt "common", "user-service" und "hello-service" anwählen
    * abschliessen


# Benutzung
## Starten der Datenbank
* z.B. über XAMPP Konsole die DB starten
## Starten der Services
* ins Root Verzeichnis wechseln
* hello-service 
    * mit Befehl ./gradlew :hello-service:bootRun hochfahren
    * oder 
    * mit Befehl ./gradlew :hello-service:build bauen und anschliessend mit java -jar hello-service/build/libs/hello-service-0.1.0.jar starten

* user-service
    * mit Befehl ./gradlew :user-service:bootRun starten

## Nutzen der Services:
* Greeting-Microservice:
    * Rest-Endpoint des hello-service aufrufen
        * http://localhost:8090/greeting?name=Samichlaus
        * gibt JSON Serialisierung des entsprechenden Greeting Objekts zurück: {"id":2,"content":"Hello, Samichlaus!"}
    * HTML UI:
        * http://localhost:8090/greeting-ui?name=Zwerg%20Lee aufrufen
            * Das gerenderte Template wird angezeigt
* User-Endpoints des user-service nutzen
    * Der User-Service bietet verschiedene Endpoints an, die ausschliesslich mit der DB verhängt sind:
        * http://localhost:8091/create?email=franz@gurke.ch&name=franz
        * http://localhost:8091/get-by-email?email=bla@gurke.ch
        * http://localhost:8091/update?id=2&email=sali@gurke.ch&name=hanswurst99
        * etc.
    * Darüber hinaus bietet der user-service einen Endpoint an, der zusätzlich intern den hello-service anspricht und dann den user in der DB anlegt.
        * http://localhost:8091/connect?myName=hans2 
                
        
# Endpoint Documentation mit Swagger
Auf dem Projekt hello-service ist probeweise mal Swagger hinzugefügt. Die Endpoint Übersicht ist hier zu finden: http://localhost:8090/swagger-ui.html

