package ch.ergon.landingapprover;

public class RunwayApprovalResultTO {

	private String icaoCode;
	private String runwayDesignator;
	private boolean isApproved;

	public RunwayApprovalResultTO(String icaoCode, String runwayDesignator, boolean isApproved) {
		super();
		this.icaoCode = icaoCode;
		this.runwayDesignator = runwayDesignator;
		this.isApproved = isApproved;
	}

	public String getIcaoCode() {
		return icaoCode;
	}

	public String getRunwayDesignator() {
		return runwayDesignator;
	}

	public boolean getIsApproved() {
		return isApproved;
	}

}
