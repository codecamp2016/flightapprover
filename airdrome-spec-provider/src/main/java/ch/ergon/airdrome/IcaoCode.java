package ch.ergon.airdrome;

public class IcaoCode {
	
	private final String icaoCode;
	
	public IcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}
	
	public String getString() {
		return icaoCode;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IcaoCode) {
			IcaoCode that = (IcaoCode) obj;
			return this.icaoCode.equals(that.icaoCode);
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return icaoCode.hashCode();
	}
}
