package ch.ergon.airdrome.runway.repository;

import java.util.List;

import ch.ergon.airdrome.IcaoCode;
import ch.ergon.airdrome.runway.RunwaySpec;
import ch.ergon.airdrome.runway.RunwaySpecKey;

public interface RunwaySpecRepository {

	List<RunwaySpecKey> getRunwaySpecKeys(IcaoCode icaoCode);

	RunwaySpec getRunwaySpec(RunwaySpecKey runwaySpecKey);

	List<IcaoCode> getIcaoCodes();

}
