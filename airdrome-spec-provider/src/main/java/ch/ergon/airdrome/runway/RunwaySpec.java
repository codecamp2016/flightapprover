package ch.ergon.airdrome.runway;

import ch.ergon.airdrome.IcaoCode;

public class RunwaySpec {

	private IcaoCode icaoCode;
	private String designator;
	private int orientation;
	
	public RunwaySpec(String icaoCode, String designator, int orientation) {
		this(new IcaoCode(icaoCode), designator, orientation);
	}
	
	public RunwaySpec(IcaoCode icaoCode, String designator, int orientation) {
		this.icaoCode = icaoCode;
		this.designator = designator;
		this.orientation = orientation;
	}
	
	public RunwaySpecKey getKey() {
		return new RunwaySpecKey(icaoCode, designator);
	}
	
	public IcaoCode getIcaoCode() {
		return icaoCode;
	}
	
	public String getDesignator() {
		return designator;
	}
	
	public int getOrientation() {
		return orientation;
	}
	
}
