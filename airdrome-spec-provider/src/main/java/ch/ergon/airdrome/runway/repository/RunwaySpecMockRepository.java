package ch.ergon.airdrome.runway.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.google.common.collect.ImmutableList;

import ch.ergon.airdrome.IcaoCode;
import ch.ergon.airdrome.runway.RunwaySpec;
import ch.ergon.airdrome.runway.RunwaySpecKey;

@Repository
public class RunwaySpecMockRepository implements RunwaySpecRepository {

	private Map<IcaoCode, List<RunwaySpecKey>> runwaySpecKeysByIcaoCode;
	private Map<RunwaySpecKey, RunwaySpec> runwaySpecByRunwaySpecKey;

	public RunwaySpecMockRepository() {
		this.runwaySpecKeysByIcaoCode = new HashMap<>();
		this.runwaySpecByRunwaySpecKey = new HashMap<>();
		addMockRunwaySpecs();
	}

	@Override
	public List<IcaoCode> getIcaoCodes() {
		return ImmutableList.copyOf(runwaySpecKeysByIcaoCode.keySet());
	}

	@Override
	public List<RunwaySpecKey> getRunwaySpecKeys(IcaoCode icaoCode) {
		return runwaySpecKeysByIcaoCode.get(icaoCode);
	}

	@Override
	public RunwaySpec getRunwaySpec(RunwaySpecKey runwaySpecKey) {
		return runwaySpecByRunwaySpecKey.get(runwaySpecKey);
	}

	private void addMockRunwaySpecs() {
		addRunwaySpec(new RunwaySpec("LSZH", "34", 340));
		addRunwaySpec(new RunwaySpec("LSZH", "16", 340));
		addRunwaySpec(new RunwaySpec("LSZH", "36", 360));
		addRunwaySpec(new RunwaySpec("LSZH", "18", 360));
		addRunwaySpec(new RunwaySpec("LSZH", "28L", 280));
		addRunwaySpec(new RunwaySpec("LSZH", "28R", 280));
		addRunwaySpec(new RunwaySpec("LSZH", "10L", 280));
		addRunwaySpec(new RunwaySpec("LSZH", "10R", 280));

		addRunwaySpec(new RunwaySpec("KJFK", "36", 360));
		addRunwaySpec(new RunwaySpec("KJFK", "18", 18));
	}

	private void addRunwaySpec(RunwaySpec runwaySpec) {
		RunwaySpecKey runwaySpecKey = runwaySpec.getKey();
		List<RunwaySpecKey> runwaySpecKeyList = getOrCreateRunwaySpecKeyList(runwaySpec.getIcaoCode());
		runwaySpecKeyList.add(runwaySpecKey);
		runwaySpecByRunwaySpecKey.put(runwaySpecKey, runwaySpec);
	}

	private List<RunwaySpecKey> getOrCreateRunwaySpecKeyList(IcaoCode icaoCode) {
		List<RunwaySpecKey> result = runwaySpecKeysByIcaoCode.get(icaoCode);
		if (result == null) {
			result = new ArrayList<>();
			runwaySpecKeysByIcaoCode.put(icaoCode, result);
		}
		return result;
	}

}
