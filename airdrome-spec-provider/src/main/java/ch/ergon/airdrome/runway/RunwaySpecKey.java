package ch.ergon.airdrome.runway;

import java.util.Objects;

import ch.ergon.airdrome.IcaoCode;

public class RunwaySpecKey {
	
	private final IcaoCode icaoCode;
	private final String runwayDesignator;
	
	public RunwaySpecKey(String icaoCode, String runwayDesignator) {
		this(new IcaoCode(icaoCode), runwayDesignator);
	}
	
	public RunwaySpecKey(IcaoCode icaoCode, String runwayDesignator) {
		this.icaoCode = icaoCode;
		this.runwayDesignator = runwayDesignator;
	}
	
	public IcaoCode getIcaoCode() {
		return icaoCode;
	}
	
	public String getRunwayDesignator() {
		return runwayDesignator;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RunwaySpecKey) {
			RunwaySpecKey that = (RunwaySpecKey) obj;
			return this.icaoCode.equals(that.icaoCode) && 
					this.runwayDesignator.equals(that.runwayDesignator);
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(icaoCode, runwayDesignator);
	}
	
}