package ch.ergon.airdrome.specprovider;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ch.ergon.airdrome.IcaoCode;
import ch.ergon.airdrome.IcaoCodeTO;
import ch.ergon.airdrome.RunwayDesignatorTO;
import ch.ergon.airdrome.RunwaySpecTO;
import ch.ergon.airdrome.runway.RunwaySpec;
import ch.ergon.airdrome.runway.RunwaySpecKey;
import ch.ergon.airdrome.runway.repository.RunwaySpecRepository;

@RestController
public class AirdromeSpecProviderController {

	private RunwaySpecRepository runwaySpecRepository;

	@Autowired
	public void setRunwaySpecRepository(RunwaySpecRepository runwaySpecRepository) {
		this.runwaySpecRepository = runwaySpecRepository;
	}

	// FIXME @ApiOperation
	@RequestMapping(value = "/airdrome/", method = RequestMethod.GET)
	public List<IcaoCodeTO> getIcaoCodes() {
		List<IcaoCode> icaoCodes = runwaySpecRepository.getIcaoCodes();
		List<IcaoCodeTO> tos = icaoCodes.stream().map(icaoCode -> new IcaoCodeTO(icaoCode.getString()))
				.collect(Collectors.toList());
		tos.stream().forEach(
				to -> to.add(linkTo(methodOn(AirdromeSpecProviderController.class).getIcaoCodes()).withSelfRel()));
		tos.stream()
				.forEach(to -> to
						.add(linkTo(methodOn(AirdromeSpecProviderController.class).getRunwayDesignators(to.getCode()))
								.withRel("runwayDesignators")));
		return tos;
	}

	// FIXME @ApiOperation
	@RequestMapping(value = "/airdrome/{icao-code}/runway", method = RequestMethod.GET)
	public List<RunwayDesignatorTO> getRunwayDesignators(@PathVariable(value = "icao-code") String icaoCode) {

		List<RunwaySpecKey> runwaySpecKeys = runwaySpecRepository.getRunwaySpecKeys(new IcaoCode(icaoCode));
		List<RunwayDesignatorTO> tos = toRunwayDesignators(runwaySpecKeys);
		tos.stream().forEach(to -> to.add(
				linkTo(methodOn(AirdromeSpecProviderController.class).getRunwayDesignators(icaoCode)).withSelfRel()));
		tos.stream().forEach(to -> to.add(linkTo(
				methodOn(AirdromeSpecProviderController.class).getRunwaySpec(to.getIcaoCode(), to.getDesignator()))
						.withRel("runwaySpec")));

		return tos;
	}

	// FIXME @ApiOperation
	@RequestMapping(value = "/airdrome/{icao-code}/runway/{runway-designator}", method = RequestMethod.GET)
	public RunwaySpecTO getRunwaySpec(@PathVariable(value = "icao-code") String icaoCode,
			@PathVariable(value = "runway-designator") String runwayDesignator) {

		RunwaySpecKey runwaySpecKey = new RunwaySpecKey(icaoCode, runwayDesignator);
		RunwaySpec runwaySpec = runwaySpecRepository.getRunwaySpec(runwaySpecKey);
		return toRunwaySpecTO(runwaySpec);
	}

	private static List<RunwayDesignatorTO> toRunwayDesignators(List<RunwaySpecKey> runwaySpecKeys) {
		return runwaySpecKeys.stream().map(runwaySpec -> new RunwayDesignatorTO(runwaySpec.getRunwayDesignator(),
				runwaySpec.getIcaoCode().getString())).collect(Collectors.toList());
	}

	private static RunwaySpecTO toRunwaySpecTO(RunwaySpec runwaySpec) {
		String icaoCode = runwaySpec.getIcaoCode().getString();
		return new RunwaySpecTO(icaoCode, runwaySpec.getDesignator(), runwaySpec.getOrientation());
	}
}
