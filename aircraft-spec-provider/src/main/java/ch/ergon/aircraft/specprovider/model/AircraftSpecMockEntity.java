package ch.ergon.aircraft.specprovider.model;

public class AircraftSpecMockEntity extends AbstractAircraftSpecEntity {
	/** The ICAO designation for the aircraft type. **/
	private String aircraftTypeDesignator;
	/** The maximum crosswind for a landing in knots. **/
	private int maxLandingCrosswind;
	
	public AircraftSpecMockEntity(AbstractAircraftSpecEntity otherAircraftSpec) {
		setAircraftTypeDesignator(otherAircraftSpec.getAircraftTypeDesignator());
		setMaxLandingCrosswind(otherAircraftSpec.getMaxLandingCrosswind());
	}
	
	public AircraftSpecMockEntity(String aircraftTypeDesignator, int maxLandingCrosswind) {
		setAircraftTypeDesignator(aircraftTypeDesignator);
		setMaxLandingCrosswind(maxLandingCrosswind);
	}
	
	@Override
	public String getAircraftTypeDesignator() {
		return aircraftTypeDesignator;
	}

	@Override
	public void setAircraftTypeDesignator(String aircraftTypeDesignator) {
		assert aircraftTypeDesignator != null;
		this.aircraftTypeDesignator = aircraftTypeDesignator;
	}

	@Override
	public int getMaxLandingCrosswind() {
		return maxLandingCrosswind;
	}

	@Override
	public void setMaxLandingCrosswind(int maxLandingCrosswind) {
		this.maxLandingCrosswind = maxLandingCrosswind;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aircraftTypeDesignator == null) ? 0 : aircraftTypeDesignator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AircraftSpecMockEntity other = (AircraftSpecMockEntity) obj;
		if (aircraftTypeDesignator == null) {
			if (other.aircraftTypeDesignator != null)
				return false;
		} else if (!aircraftTypeDesignator.equals(other.aircraftTypeDesignator))
			return false;
		return true;
	}
}
