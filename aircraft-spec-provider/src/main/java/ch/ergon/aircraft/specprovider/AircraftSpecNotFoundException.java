package ch.ergon.aircraft.specprovider;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class AircraftSpecNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -3092974235775515249L;

	public AircraftSpecNotFoundException() {
		super();
	}
	
	public AircraftSpecNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public AircraftSpecNotFoundException(String message) {
			super(message);
	}

	public AircraftSpecNotFoundException(Throwable cause) {
		super(cause);
	}
}
