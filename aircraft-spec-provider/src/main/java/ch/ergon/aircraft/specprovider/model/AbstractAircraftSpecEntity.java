package ch.ergon.aircraft.specprovider.model;

public abstract class AbstractAircraftSpecEntity {
	public abstract String getAircraftTypeDesignator();
	
	public abstract void setAircraftTypeDesignator(String aircraftTypeDesignator);
	
	public abstract int getMaxLandingCrosswind();
	
	public abstract void setMaxLandingCrosswind(int maxLandingCrosswind);
}