package ch.ergon.aircraft.specprovider;

import static ch.ergon.aircraft.specprovider.AircraftSpecMapper.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ergon.aircraft.AircraftSpecTO;
import ch.ergon.aircraft.specprovider.model.AircraftSpecRepository;

@RestController
// TODO bmorandi: Add support for different crosswinds for different runway conditions.
public class AircraftSpecProviderController {
	@RequestMapping(value = "/aircraft/{aircraftTypeDesignator}", method=RequestMethod.GET)
	public AircraftSpecTO aircraftSpec(@PathVariable(value = "aircraftTypeDesignator") String aircraftTypeDesignator) {
		return aircraftSpecRepository.getAircraftSpecByAircraftTypeDesignator(aircraftTypeDesignator)
			.map(abstractAircraftSpecEntity -> map(abstractAircraftSpecEntity))
			.orElseThrow(() -> new AircraftSpecNotFoundException("Aircraft type designator unknown."));
	}

	@Autowired
	private AircraftSpecRepository aircraftSpecRepository;
}
