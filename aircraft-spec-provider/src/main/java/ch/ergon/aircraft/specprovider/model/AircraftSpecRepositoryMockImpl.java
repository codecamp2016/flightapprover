package ch.ergon.aircraft.specprovider.model;

import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.google.common.collect.ImmutableMap;

@Repository
public class AircraftSpecRepositoryMockImpl implements AircraftSpecRepository {
	private Map<String, AbstractAircraftSpecEntity> aircraftSpecs;
	
	public AircraftSpecRepositoryMockImpl() {
		init();
	}
	
	private void init() {
		aircraftSpecs = ImmutableMap.<String, AbstractAircraftSpecEntity>builder()
			.put("A320", new AircraftSpecMockEntity("A320", 20))
			.put("CL60", new AircraftSpecMockEntity("CL60", 5))
			.build();
	}
	
	@Override
	public void createAircraftSpec(AbstractAircraftSpecEntity aircraftSpec) {
		aircraftSpecs.put(aircraftSpec.getAircraftTypeDesignator(), new AircraftSpecMockEntity(aircraftSpec));
	}

	@Override
	public void deleteAircraftSpec(AbstractAircraftSpecEntity aircraftSpec) {
		aircraftSpecs.remove(aircraftSpec);	
	}

	@Override
	public Optional<AbstractAircraftSpecEntity> getAircraftSpecByAircraftTypeDesignator(String aircraftTypeDesignator) {
		return Optional.ofNullable(aircraftSpecs.get(aircraftTypeDesignator));
	}

	@Override
	public void update(AbstractAircraftSpecEntity aircraftSpec) {
		aircraftSpecs.put(aircraftSpec.getAircraftTypeDesignator(), new AircraftSpecMockEntity(aircraftSpec));
	}
}
