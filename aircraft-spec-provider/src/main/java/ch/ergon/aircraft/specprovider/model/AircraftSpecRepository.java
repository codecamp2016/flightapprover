package ch.ergon.aircraft.specprovider.model;

import java.util.Optional;

public interface AircraftSpecRepository {  
	  public void createAircraftSpec(AbstractAircraftSpecEntity aircraftSpec);

	  public void deleteAircraftSpec(AbstractAircraftSpecEntity aircraftSpec);
	  
	  public Optional<AbstractAircraftSpecEntity> getAircraftSpecByAircraftTypeDesignator(String aircraftTypeDesignator);

	  public void update(AbstractAircraftSpecEntity aircraftSpec);
}