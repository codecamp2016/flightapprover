package ch.ergon.aircraft.specprovider;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import com.google.common.base.Predicates;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
public class AircraftSpecProvider {

	public static void main(String[] args) {
		SpringApplication.run(AircraftSpecProvider.class, args);
	}

	@SuppressWarnings("unchecked")
	@Bean
	public Docket aircraftSpecProviderApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(Predicates.and(Predicates.not(regex("/admin.*")), Predicates.not(regex("/admin/.*")),
						Predicates.not(regex("/error"))))
				.build();
	}

}
