package ch.ergon.aircraft.specprovider;

import ch.ergon.aircraft.AircraftSpecTO;
import ch.ergon.aircraft.specprovider.model.AbstractAircraftSpecEntity;

public class AircraftSpecMapper {
	// FIXME bmorandi: Think about absent case.
	public static AircraftSpecTO map(AbstractAircraftSpecEntity aircraftSpec) {
		return new AircraftSpecTO(aircraftSpec.getAircraftTypeDesignator(), aircraftSpec.getMaxLandingCrosswind());
	}
}
