package user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import common.Greeting;

@Controller
public class MainController {

	private UserController userController;

	@Autowired
	public void setUserController(UserController userController) {
		this.userController = userController;
	}

	@RequestMapping("/")
	@ResponseBody
	public String index() {
		return "Proudly handcrafted by " + "<a href='http://netgloo.com/en'>netgloo</a> :)";
	}

	@RequestMapping("/connect")
	@ResponseBody
	public String connect(String myName) {

		RestTemplate restTemplate = new RestTemplate();
		Greeting greeting = restTemplate.getForObject("http://localhost:8090/greeting?name=" + myName, Greeting.class);

		getUserController().create("sali" + myName + "@gurke.ch", myName);
		return greeting.getContent();
	}

	public UserController getUserController() {
		return userController;
	}

}
