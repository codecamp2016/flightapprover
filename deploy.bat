# stop running containers
docker-compose stop

# Clean up existing containers and images
docker-compose rm -f
docker rmi -f $(docker images -q) 

# Build new jars
./gradlew build

# Start docker machines
docker-compose up --build
