package ch.ergon.airdrome;

import org.springframework.hateoas.ResourceSupport;

public class RunwayDesignatorTO extends ResourceSupport {

	private String designator;
	private String icaoCode;

	public RunwayDesignatorTO() {
		super();
	}

	public RunwayDesignatorTO(String designator, String icaoCode) {
		super();
		this.designator = designator;
		this.icaoCode = icaoCode;
	}

	public String getDesignator() {
		return designator;
	}

	public void setDesignator(String designator) {
		this.designator = designator;
	}

	public String getIcaoCode() {
		return icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	@Override
	public String toString() {
		return "RunwayDesignatorTO [designator=" + designator + ", icaoCode=" + icaoCode + ", getLinks()=" + getLinks()
				+ "]";
	}

}
