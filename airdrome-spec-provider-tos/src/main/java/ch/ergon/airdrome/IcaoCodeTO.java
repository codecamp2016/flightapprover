package ch.ergon.airdrome;

import org.springframework.hateoas.ResourceSupport;

public class IcaoCodeTO extends ResourceSupport {

	private String code;

	public IcaoCodeTO() {
		super();
	}

	public IcaoCodeTO(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "IcaoCodeTO [code=" + code + ", getLinks()=" + getLinks() + "]";
	}

}
