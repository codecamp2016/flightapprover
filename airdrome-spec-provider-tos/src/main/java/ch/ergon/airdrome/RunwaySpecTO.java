package ch.ergon.airdrome;

public class RunwaySpecTO {

	private String icaoCode;
	private String designator;
	private int orientation;

	public RunwaySpecTO() {
		super();
	}

	public RunwaySpecTO(String icaoCode, String designator, int orientation) {
		this.icaoCode = icaoCode;
		this.designator = designator;
		this.orientation = orientation;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public void setDesignator(String designator) {
		this.designator = designator;
	}

	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	public String getIcaoCode() {
		return icaoCode;
	}

	public String getDesignator() {
		return designator;
	}

	public int getOrientation() {
		return orientation;
	}

	@Override
	public String toString() {
		return "RunwaySpecTO [icaoCode=" + icaoCode + ", designator=" + designator + ", orientation=" + orientation
				+ "]";
	}

}
