package ch.ergon.landingapprover;

import static java.lang.Math.abs;
import static java.lang.Math.round;
import static java.lang.Math.sin;

public class WindCalculatorService {	
	public static long crosswindComponent(int windSourceDirection, int windStrength, int runwayHeading) {
		int angle = runwayHeading >= windSourceDirection ? runwayHeading - windSourceDirection : windSourceDirection - runwayHeading;
		return abs(round(sin(angle * Math.PI / 180) * windStrength));
	}
}
