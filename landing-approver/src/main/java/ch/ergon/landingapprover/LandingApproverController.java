package ch.ergon.landingapprover;

import static ch.ergon.landingapprover.WindCalculatorService.crosswindComponent;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.collect.ImmutableList;

import ch.ergon.aircraft.AircraftSpecTO;
import ch.ergon.airdrome.RunwayDesignatorTO;
import ch.ergon.airdrome.RunwaySpecTO;
import ch.ergon.landingapprover.client.AircraftSpecClient;
import ch.ergon.landingapprover.client.AirdromeSpecClient;
import ch.ergon.landingapprover.client.MeteoInfoClient;
import ch.ergon.meteo.MeteoInformationTO;

@Controller
public class LandingApproverController {

	private AircraftSpecClient aircraftSpecClient;
	private MeteoInfoClient meteoInfoClient;
	private AirdromeSpecClient airdromeSpecClient;

	@Autowired
	private LandingApproverController(MeteoInfoClient meteoInfoClient, AircraftSpecClient aircraftSpecClient,
			AirdromeSpecClient airdromeSpecClient) {
		super();
		this.aircraftSpecClient = aircraftSpecClient;
		this.meteoInfoClient = meteoInfoClient;
		this.airdromeSpecClient = airdromeSpecClient;
	}

	@RequestMapping(value = "/landing-approver-result", method = RequestMethod.GET)
	public String approveLanding(@RequestParam(value = "aircraftTypeDesignator") String aircraftTypeDesignator,
			@RequestParam(value = "icaoCode") String icaoCode, Model model) {

		List<RunwayApprovalResultTO> results = approveLanding(aircraftTypeDesignator, icaoCode);

		model.addAttribute("icaoCode", icaoCode);
		model.addAttribute("aircraftTypeDesignator", aircraftTypeDesignator);
		model.addAttribute("runwayApprovalResults", results);
		return "landing-approver-result";
	}

	private List<RunwayApprovalResultTO> approveLanding(String aircraftTypeDesignator, String icaoCode) {
		List<RunwayApprovalResultTO> results = new ArrayList<>();

		MeteoInformationTO meteoInfo = meteoInfoClient.getMeteoInfo(icaoCode);
		AircraftSpecTO aircraftSpec = aircraftSpecClient.getAircraftSpec(aircraftTypeDesignator);

		List<RunwayDesignatorTO> runwayDesignators = airdromeSpecClient.getRunwayDesignators(icaoCode);

		for (RunwayDesignatorTO runwayDesignator : runwayDesignators) {
			RunwaySpecTO runwaySpec = airdromeSpecClient.getRunwaySpec(runwayDesignator);
			results.add(approveLandingForRunway(meteoInfo, aircraftSpec, runwaySpec));
		}

		return results;
	}

	private RunwayApprovalResultTO approveLandingForRunway(MeteoInformationTO meteoInfo, AircraftSpecTO aircraftSpec,
			RunwaySpecTO runwaySpec) {
		if (aircraftSpec.getMaxLandingCrosswind() >= crosswindComponent(meteoInfo.getWindSpeedDirection(),
				meteoInfo.getWindSpeedVelocity(), runwaySpec.getOrientation())) {
			return new RunwayApprovalResultTO(runwaySpec.getIcaoCode(), runwaySpec.getDesignator(), true);
		} else {
			return new RunwayApprovalResultTO(runwaySpec.getIcaoCode(), runwaySpec.getDesignator(), false);
		}
	}

	private static ImmutableList<RunwayApprovalResultTO> getMockLandingApprovalResults(String icaoCode) {
		return ImmutableList.of(new RunwayApprovalResultTO(icaoCode, "27L", true),
				new RunwayApprovalResultTO(icaoCode, "09L", false), new RunwayApprovalResultTO(icaoCode, "27R", true),
				new RunwayApprovalResultTO(icaoCode, "09R", false));
	}

}
