package ch.ergon.landingapprover.client;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ergon.meteo.MeteoInformationTO;

@Component
public class MeteoInfoClient {

	private RestTemplate restTemplate;
	private String meteoInfoServiceHost;
	private long meteoInfoServicePort;
	private boolean useRibbon;
	private LoadBalancerClient loadBalancer;

	@Autowired
	public MeteoInfoClient(
			@Value("${meteo-info-provider.service.host:meteo-info-provider}") String meteoInfoServiceHost,
			@Value("${meteo-info-provider.service.port:9001}") long meteoInfoServicePort,
			@Value("${ribbon.eureka.enabled:false}") boolean useRibbon) {
		super();
		this.restTemplate = getRestTemplate();
		this.meteoInfoServiceHost = meteoInfoServiceHost;
		this.meteoInfoServicePort = meteoInfoServicePort;
		this.useRibbon = false; // FIXME
	}

	@Autowired
	public void setLoadBalancer(LoadBalancerClient loadBalancer) {
		this.loadBalancer = loadBalancer;
	}

	protected RestTemplate getRestTemplate() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.registerModule(new Jackson2HalModule());

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Arrays.asList(MediaTypes.HAL_JSON));
		converter.setObjectMapper(mapper);

		return new RestTemplate(Collections.<HttpMessageConverter<?>> singletonList(converter));
	}

	private String meteoInfoURL() {
		String url;
		if (useRibbon) {
			ServiceInstance instance = loadBalancer.choose("meteo-info-provider");
			url = "http://" + instance.getHost() + ":" + instance.getPort() + "/meteo-info/";

		} else {
			url = "http://" + meteoInfoServiceHost + ":" + meteoInfoServicePort + "/meteo-info/";
		}
		return url;

	}

	public MeteoInformationTO getMeteoInfo(String icaoCode) {
		MeteoInformationTO meteoInfo = restTemplate.getForObject(meteoInfoURL() + icaoCode, MeteoInformationTO.class);
		return meteoInfo;
	}

}
