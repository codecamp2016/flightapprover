package ch.ergon.landingapprover.client;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ergon.airdrome.RunwayDesignatorTO;
import ch.ergon.airdrome.RunwaySpecTO;

@Component
public class AirdromeSpecClient {

	private RestTemplate restTemplate;
	private String airdromeSpecServiceHost;
	private long airdromeSpecServicePort;
	private boolean useRibbon;
	private LoadBalancerClient loadBalancer;

	@Autowired
	public AirdromeSpecClient(
			@Value("${airdrome-spec-provider.service.host:airdrome-spec-provider}") String airdromeSpecServiceHost,
			@Value("${airdrome-spec-provider.service.port:9003}") long airdromeSpecServicePort,
			@Value("${ribbon.eureka.enabled:false}") boolean useRibbon) {
		super();
		this.restTemplate = getRestTemplate();
		this.airdromeSpecServiceHost = airdromeSpecServiceHost;
		this.airdromeSpecServicePort = airdromeSpecServicePort;
		this.useRibbon = false; // FIXME
	}

	@Autowired
	public void setLoadBalancer(LoadBalancerClient loadBalancer) {
		this.loadBalancer = loadBalancer;
	}

	protected RestTemplate getRestTemplate() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.registerModule(new Jackson2HalModule());

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Arrays.asList(MediaTypes.HAL_JSON));
		converter.setObjectMapper(mapper);

		return new RestTemplate(Collections.<HttpMessageConverter<?>> singletonList(converter));
	}

	private String airdromeURL() {
		String url;
		if (useRibbon) {
			ServiceInstance instance = loadBalancer.choose("airdrome-spec-provider");
			url = "http://" + instance.getHost() + ":" + instance.getPort() + "/airdrome/";

		} else {
			url = "http://" + airdromeSpecServiceHost + ":" + airdromeSpecServicePort + "/airdrome/";
		}
		return url;

	}

	public List<RunwayDesignatorTO> getRunwayDesignators(String icaoCode) {
		String url = airdromeURL() + icaoCode + "/runway/";
		ResponseEntity<RunwayDesignatorTO[]> responseEntity = restTemplate.getForEntity(url,
				RunwayDesignatorTO[].class);
		RunwayDesignatorTO[] runwayDesignatorTOs = responseEntity.getBody();

		return Arrays.asList(runwayDesignatorTOs);
	}

	public RunwaySpecTO getRunwaySpec(RunwayDesignatorTO runwayDesignator) {
		RunwaySpecTO runwaySpec = restTemplate.getForObject(
				airdromeURL() + runwayDesignator.getIcaoCode() + "/runway/" + runwayDesignator.getDesignator(),
				RunwaySpecTO.class);
		return runwaySpec;
	}

}
