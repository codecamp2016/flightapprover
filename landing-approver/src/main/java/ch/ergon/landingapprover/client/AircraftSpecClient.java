package ch.ergon.landingapprover.client;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ergon.aircraft.AircraftSpecTO;

@Component
public class AircraftSpecClient {

	private RestTemplate restTemplate;
	private String aircraftSpecServiceHost;
	private long aircraftSpecServicePort;
	private boolean useRibbon;
	private LoadBalancerClient loadBalancer;

	@Autowired
	public AircraftSpecClient(
			@Value("${aircraft-spec-provider.service.host:aircraft-spec-provider}") String aircraftSpecServiceHost,
			@Value("${aircraft-spec-provider.service.port:9002}") long aircraftSpecServicePort,
			@Value("${ribbon.eureka.enabled:false}") boolean useRibbon) {
		super();
		this.restTemplate = getRestTemplate();
		this.aircraftSpecServiceHost = aircraftSpecServiceHost;
		this.aircraftSpecServicePort = aircraftSpecServicePort;
		this.useRibbon = false; // FIXME
	}

	@Autowired
	public void setLoadBalancer(LoadBalancerClient loadBalancer) {
		this.loadBalancer = loadBalancer;
	}

	protected RestTemplate getRestTemplate() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.registerModule(new Jackson2HalModule());

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Arrays.asList(MediaTypes.HAL_JSON));
		converter.setObjectMapper(mapper);

		return new RestTemplate(Collections.<HttpMessageConverter<?>> singletonList(converter));
	}

	private String customerURL() {
		String url;
		if (useRibbon) {
			ServiceInstance instance = loadBalancer.choose("aircraft-spec-provider");
			url = "http://" + instance.getHost() + ":" + instance.getPort() + "/aircraft/";

		} else {
			url = "http://" + aircraftSpecServiceHost + ":" + aircraftSpecServicePort + "/aircraft/";
		}
		return url;

	}

	public AircraftSpecTO getAircraftSpec(String aircraftTypeDesignator) {
		AircraftSpecTO aircraftSpec = restTemplate.getForObject(customerURL() + aircraftTypeDesignator,
				AircraftSpecTO.class);
		return aircraftSpec;
	}

}
