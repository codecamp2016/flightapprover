package ch.ergon.meteo;

import java.util.Date;

public class MeteoInformationTO {

	public MeteoInformationTO() {
		super();
	}

	public MeteoInformationTO(String airdromeDesignator, Date time, int windSpeedVelocity, int windSpeedDirection) {
		super();
		this.icaoCode = airdromeDesignator;
		this.time = time;
		this.windSpeedVelocity = windSpeedVelocity;
		this.windSpeedDirection = windSpeedDirection;
	}

	private String icaoCode;
	private Date time;
	private int windSpeedVelocity;
	private int windSpeedDirection;

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setWindSpeedVelocity(int windSpeedVelocity) {
		this.windSpeedVelocity = windSpeedVelocity;
	}

	public void setWindSpeedDirection(int windSpeedDirection) {
		this.windSpeedDirection = windSpeedDirection;
	}

	public String getIcaoCode() {
		return icaoCode;
	}

	public Date getTime() {
		return time;
	}

	public int getWindSpeedVelocity() {
		return windSpeedVelocity;
	}

	public int getWindSpeedDirection() {
		return windSpeedDirection;
	}

	@Override
	public String toString() {
		return "MeteoInformationTO [icaoCode=" + icaoCode + ", time=" + time + ", windSpeedVelocity="
				+ windSpeedVelocity + ", windSpeedDirection=" + windSpeedDirection + "]";
	}

}
