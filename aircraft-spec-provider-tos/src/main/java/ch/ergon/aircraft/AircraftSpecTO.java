package ch.ergon.aircraft;

public class AircraftSpecTO {
	/** The ICAO designation for the aircraft type. **/
	private String aircraftTypeDesignator;
	/** The maximum crosswind for a landing in knots. **/
	private int maxLandingCrosswind;

	public AircraftSpecTO() {
		super();
	}

	public AircraftSpecTO(String aircraftTypeDesignator, int maxLandingCrosswind) {
		this.aircraftTypeDesignator = aircraftTypeDesignator;
		this.maxLandingCrosswind = maxLandingCrosswind;
	}

	public String getAircraftTypeDesignator() {
		return aircraftTypeDesignator;
	}

	public void setAircraftTypeDesignator(String aircraftTypeDesignator) {
		this.aircraftTypeDesignator = aircraftTypeDesignator;
	}

	public int getMaxLandingCrosswind() {
		return maxLandingCrosswind;
	}

	public void setMaxLandingCrosswind(int maxLandingCrosswind) {
		this.maxLandingCrosswind = maxLandingCrosswind;
	}

	@Override
	public String toString() {
		return "AircraftSpecTO [aircraftTypeDesignator=" + aircraftTypeDesignator + ", maxLandingCrosswind="
				+ maxLandingCrosswind + "]";
	}

}